const express = require('express');
const { OAuth2Client } = require('google-auth-library');
const User = require('./model/User');

const router = express.Router();

const clientId = process.env.GOOGLE_CLIENT_ID;
const client = new OAuth2Client(clientId);

router.post('/login', async (req, res) => {
	const { email, password } = req.body;

	try {
		const user = await User.findOne({ email });
		if (!user) {
			res.status(401).send('Invalid email or password');
			return;
		}

		const isPasswordValid = await user.comparePassword(password);
		if (!isPasswordValid) {
			res.status(401).send('Invalid email or password');
			return;
		}

		req.session.user = user;
	} catch (error) {
		res.status(500).send(error);
	}
});

router.post('/google', async (req, res) => {
	const { idToken } = req.body;

	try {
		const ticket = await client.verifyIdToken({
			idToken,
			audience: clientId,
		});
		const { email } = ticket.getPayload();

		let user = await User.findOne({ email });
		if (!user) {
			user = new User({ email });
			await user.save();
		}

		req.session.user = user;
		res.send(user);
	} catch (error) {
		res.status(401).send(error);
	}
});

router.post('/logout', (req, res) => {
	req.session.destroy((error) => {
		if (error) {
			res.status(500).send(error);
		} else {
			res.send();
		}
	});
});

router.post('/register', async (req, res) => {
	const { email, password } = req.body;
	console.log(req.body);
	try {
		let user = new User({ email, password });
		await user.save();
		req.session.user = user;
		res.send(user);
	} catch (error) {
		res.status(500).send(error);
	}
});

module.exports = router;
