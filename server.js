const express = require('express');
const session = require('express-session');
const MongoStore = require('connect-mongo');
const mongoose = require('mongoose');
const cors = require('cors');

const { MONGO_USERNAME = '', MONGO_PASSWORD = '', MONGO_HOST = 'localhost', MONGO_PORT = 27017, MONGO_DATABASE = 'auth_test' } = process.env;

const MONGO_URI =
	MONGO_USERNAME && MONGO_PASSWORD
		? `mongodb://${MONGO_USERNAME}:${encodeURIComponent(MONGO_PASSWORD)}@${MONGO_HOST}:${MONGO_PORT}/${MONGO_DATABASE}`
		: `mongodb://${MONGO_HOST}:${MONGO_PORT}/${MONGO_DATABASE}`;

const app = express();

// Connect to MongoDB
mongoose.connect(MONGO_URI, {
	useNewUrlParser: true,
	useUnifiedTopology: true,
});

app.use(
	session({
		secret: '123456',
		resave: false,
		saveUninitialized: false,
		store: MongoStore.create({ mongoUrl: MONGO_URI }),
	})
);
app.use(cors({ origin: 'http://127.0.0.1:5500' }));
app.use(cors({ origin: '*' }));
app.use(express.json());

// routes go here

const port = process.env.PORT || 3000;
app.listen(port, () => {
	const authRouter = require('./auth');
	app.use('/auth', authRouter);
	console.log(`Server listening on port ${port}`);
});
